package sdsu.cs657.neuralNetwork;

import java.util.ArrayList;

public class SynapseList {

	private ArrayList<Synapse> synapses = new ArrayList<Synapse>();
	
	public void add(Synapse synapse){
		synapses.add(synapse);
	}
	
	public double sum(){
		double sum = 0;
		for(Synapse each: synapses)
			sum += each.output();
		return sum;
	}
	
	public void propogate(){
		for(Synapse each: synapses)
			each.propogate();
	}
	
	public double sumOfErrorGradients(){
		double sum = 0;
		for(Synapse each : synapses)
			sum += each.getWeight() * each.to().errorGradient();
		return sum;
	}
	
	public String toString(){
		String result = "";
		for(Synapse each: synapses) 
			result += each.toString();
		return result;
	}
}
