package sdsu.cs657.neuralNetwork;

import java.util.ArrayList;

public class TrainingSet {
    private ArrayList<TrainingInput> inputs = new ArrayList<TrainingInput>();
    private ArrayList<TrainingOutput> outputs = new ArrayList<TrainingOutput>();
    
    private int currentIndex = -1;
    
    public TrainingSet (ArrayList<TrainingInput> inputs, ArrayList<TrainingOutput> outputs) {
        this.inputs = inputs;
        this.outputs = outputs;
    }
    
    public boolean next () {
        currentIndex++;
        if (currentIndex >= size()) {
            currentIndex = 0;
        }
        return true;
    }
    
    public int size () {
        return inputs.size();
    }
    
    public TrainingOutput output()
    {
        return outputs.get(currentIndex);
    }
    
    public TrainingInput input()
    {
        return inputs.get(currentIndex);
    }

}
