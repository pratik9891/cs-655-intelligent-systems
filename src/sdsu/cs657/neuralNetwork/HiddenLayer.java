package sdsu.cs657.neuralNetwork;

public class HiddenLayer extends NeuralLayer{
	
	NeuralLayer previous;

	public HiddenLayer(int index, int numberOfNeurons) {
		super("Hidden"+index,numberOfNeurons,new ActivationFunction());
	}
	
	public void propagate(){
		for(int i=0; i<neurons.size(); i++) {
			Neuron neuron = neurons.get(i);
			double sumOfErrorGradientToNextLayer = neuron.sumOfOutputSynapsesErrorGradients();
			double errorGradient = neuron.output() * (1-neuron.output()) * sumOfErrorGradientToNextLayer;
			neuron.setErrorGradient(errorGradient);
			neuron.propogate();
		}
	}

}
