package sdsu.cs657.neuralNetwork;

public class ActivationFunction {
	
	 public double calculate(double x) {
		    return (1 / (1 + Math.exp(-x)));
	 }
}
