package sdsu.cs657.neuralNetwork;

public class OutputLayer extends NeuralLayer {
	
	private double error;

	public OutputLayer(int numberOfNeurons) {
		super("Output", numberOfNeurons, new ActivationFunction());
	}
	
	public double error(){
		return this.error;
	}
	
	 public void propagate (TrainingOutput trainingOutput)
	    {
	        double error = 0;
	        for (int i = 0; i<neurons.size(); i++) {
	            Neuron neuron = neurons.get(i);
	            
	            double diff = trainingOutput.get(i) - neuron.output();
	          //  System.out.println("diff :: " + neuron.output() +" - "+trainingOutput.get(i)+" = "+diff);
	            error = Math.pow(diff, 2);
	            
	            double errorGradient = neuron.output() * (1 - neuron.output()) * (trainingOutput.get(i)-neuron.output());
	            
	            neuron.setErrorGradient(errorGradient);
	            
	            neuron.propogate();
	        }
	        
	        this.error = error;
	    }
	 
	 public double value(){
		 return this.neurons.get(0).output();
	 }

}

