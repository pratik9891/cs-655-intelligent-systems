package sdsu.cs657.neuralNetwork;

import java.util.ArrayList;

public class NeuralNetwork {
	
	private InputLayer inputLayer;
	private OutputLayer outputLayer;
	private ArrayList<HiddenLayer> hiddenLayers = new ArrayList<HiddenLayer>();
	private TrainingSet training;
	
	public NeuralNetwork(int... counts) //neuron counts for each layer
	{
		NeuralLayer previousLayer = null;
		NeuralLayer currentLayer = null;
		for(int i=0; i<counts.length; i++){
			if(i==0){
				inputLayer = new InputLayer(counts[i]);
				currentLayer = inputLayer;
			} else if (i == (counts.length - 1)){
				outputLayer = new OutputLayer(counts[i]);
				currentLayer = outputLayer;
			} else {
				HiddenLayer layer = new HiddenLayer(i,counts[i]);
				hiddenLayers.add(layer);
				currentLayer = layer;
			}
			
			//connect the layers
			if( previousLayer != null)
				previousLayer.connect(currentLayer);
			
			previousLayer = currentLayer;
		}
	}
	
	public void train(TrainingSet trainingSet) {
		this.training = trainingSet;
		System.out.println("Training!");
		double error = 0;
		for(int k =0 ; k <5000; k++) {
			training.next();
			inputLayer.input(training.input());
			for (int i = 0; i < hiddenLayers.size(); i++) {
                HiddenLayer layer = hiddenLayers.get(i);
                layer.execute();
            }
			outputLayer.execute();

			//learning
			outputLayer.propagate(training.output());
            
            for (int i = hiddenLayers.size() - 1; i >= 0 ; i--) {
                HiddenLayer layer = hiddenLayers.get(i);
                layer.propagate();
            }
            error += outputLayer.error();
            if(k%training.size() == 0){
            	if(k==0)
            		continue;
            	System.out.println(error);
            	error=0;      	
            }
//            if(k%training.size() == 0){
//            	System.out.println("Interation:"+k + "e:"+error);
//            	error =0;
//            }
		}	
	}
	
	public void ask (TrainingSet trainingSet) {
        this.training = trainingSet;
        System.out.println("TEST");
        int counter = 0;
       while(counter < trainingSet.size()){
        
        trainingSet.next();
        inputLayer.input(trainingSet.input());

        for (int i = 0; i < hiddenLayers.size(); i++) {
            HiddenLayer layer = hiddenLayers.get(i);
            layer.execute();
        }
        
        outputLayer.execute();
        double error = outputLayer.value() - trainingSet.output().get(0);
        System.out.println("Output:"+outputLayer.value()*5 + " Error:"+error); 
        //System.out.println(error);
        counter++;
       }
    }
	
	public static void main(String[] args){
		NeuralNetwork test = new NeuralNetwork(1,2,2,1);
		ArrayList<TrainingInput> inputs = new ArrayList<TrainingInput>();
		ArrayList<TrainingOutput> outputs = new ArrayList<TrainingOutput>();
		Math.max(10,2);		
		for(double i=0.5; i<5; i=i+0.1){
			inputs.add(new TrainingInput(NormalizeInput((i*i*i + 2*i*i -2*i +1))));
			outputs.add(new TrainingOutput(NormalizeOutput(i)));
		}	
		
	//  !!! for XOR!!!!
//		inputs.add(new TrainingInput(1,1));
//		outputs.add(new TrainingOutput(0));
//		inputs.add(new TrainingInput(0,0));
//		outputs.add(new TrainingOutput(0));
//		inputs.add(new TrainingInput(1,0));
//		outputs.add(new TrainingOutput(1));
//		inputs.add(new TrainingInput(0,1));
//		outputs.add(new TrainingOutput(1));


		TrainingSet trainingSet = new TrainingSet(inputs, outputs);
		
		test.train(trainingSet);
		inputs.clear();
		outputs.clear();
		for(int i=0;i<100;i++){
			double number = (Math.random()*5);
			inputs.add(new TrainingInput(NormalizeInput(number*number*number+2*number*number-2*number+1)));
			outputs.add(new TrainingOutput(NormalizeOutput(number)));
			trainingSet = new TrainingSet(inputs, outputs);
		}
		
		
//		testInput.generate();
//		test.run(testInput);
		
//		inputs = new ArrayList<TrainingInput>();
        test.ask(trainingSet);
	}
	
	public static double NormalizeInput(Double value){
		return (value-0.625)/(165-0.625);
	}
	
	public static double NormalizeOutput(Double value){
		return (value-0.5)/(5.0-0.5);
	}
	
	
}
