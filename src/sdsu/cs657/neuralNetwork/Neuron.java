package sdsu.cs657.neuralNetwork;

import java.util.ArrayList;

public class Neuron {
	
	private ActivationFunction function;
	private SynapseList inputs = new SynapseList();
	private SynapseList outputs = new SynapseList();	
	private double output;
	private double theta = ((Math.random() * 1) - 0.5);
	private double previousThetaDelta = 0;
	private double errorGradient;
	
	String name;//debug purposes
	
	public Neuron(String name,  ActivationFunction function){
		this.name = name;
		this.function = function;
	}
	
	//used for inputlayer only
	public void setInitialOutput(double value){
		this.output = value;
	}
	
	public double output(){
		return output;
	}
	
	public void setErrorGradient(Double errorGradient){
		this.errorGradient = errorGradient;
	}
	
	public double sumOfOutputSynapsesErrorGradients(){
		return outputs.sumOfErrorGradients();
	}
	
	public double errorGradient(){
		return this.errorGradient;
	}
	
	public void connect (Neuron next){
		Synapse newWeight = new Synapse(this, next);
		outputs.add(newWeight);
		next.inputs.add(newWeight);
	}
	
	public double execute(){
		output = function.calculate(inputs.sum() - theta);
		return output;
	}
	
	public void propogate(){
		//changing theta!
		double currentDeltaTheta = Constants.TRAINING_RATE *-1* this.errorGradient + Constants .MOMENTUM*previousThetaDelta;
		this.theta += currentDeltaTheta;
		this.previousThetaDelta = currentDeltaTheta;
		//changing weights!
		inputs.propogate();
	}
	
	public String toString(){
		return name+" : output = " + this.execute();
	}

}
