package sdsu.cs657.neuralNetwork;

import java.util.ArrayList;

public class TrainingOutput
{
    private ArrayList<Double> outputs = new ArrayList<Double>();
    
    public TrainingOutput (double... outputValues)
    {
        for (double value : outputValues) {
            outputs.add(value);
        }
    }

    public Double get (int i)
    {
        return outputs.get(i);
    }
    
    public void add (Double value)
    {
        outputs.add(value);
    }
    
    public int size(){
    	return outputs.size(); 			
    }

}
