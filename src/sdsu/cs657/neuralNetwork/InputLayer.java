package sdsu.cs657.neuralNetwork;

import java.util.ArrayList;

public class InputLayer extends NeuralLayer {

	public InputLayer(int numberOfNeurons) {
		super("Input", numberOfNeurons, new Linear());
	}
	
//	public void input(TrainingInput input){
//		for(int i=0; i<neurons.size(); i++)
//			neurons.get(i).setInitialOutput(input.get());
//	}
//	
//	public void input(TestInput test){
//		for(int i=0; i<neurons.size(); i++)
//			neurons.get(i).setInitialOutput(test.get());
//	}
	
	public void input(TrainingInput input)
    {
        for (int i = 0; i<neurons.size(); i++) {
            neurons.get(i).setInitialOutput(input.get(i));
        }
    }
}
