package sdsu.cs657.neuralNetwork;

public class ModifiedSigmoidal extends ActivationFunction {

	public double calculate(double x) {
	    return (5 / (1 + Math.exp(-x)));
 }
}
