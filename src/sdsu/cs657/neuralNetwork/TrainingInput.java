package sdsu.cs657.neuralNetwork;

import java.util.ArrayList;

public class TrainingInput
{
    private ArrayList<Double> inputs = new ArrayList<Double>();
    
    public TrainingInput (double... inputValues) {
        for (double value : inputValues) {
            inputs.add(value);
        }
    }

    public Double get (int i)
    {
        return inputs.get(i);
    }
    
    public TrainingInput add (Double value)
    {
        inputs.add(value);
        return this;
    }
    
    public int size(){
    	return inputs.size();
    }

}
