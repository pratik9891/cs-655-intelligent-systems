package sdsu.cs657.neuralNetwork;

import java.util.ArrayList;

public class NeuralLayer {

	String name; //for debug purposes
	protected ArrayList<Neuron> neurons = new ArrayList<Neuron>();
	private NeuralLayer next, previous;
	
	
	public NeuralLayer(String name, int numberOfNeurons, ActivationFunction function){
		this.name = name;
		for(int i=0; i<numberOfNeurons; i++) 
			neurons.add(new Neuron(name+"N"+i, function));
	}
	
	public void execute(){
		for(Neuron each : neurons)
			each.execute();
	}
	
	public void add(Neuron neuron){
		neurons.add(neuron);
	}

	public void connect (NeuralLayer nextLayer){
		nextLayer.previous = this;
		this.next = nextLayer;
		for(Neuron each:neurons)
			this.next.connect(each);
	}
	
	public void connect(Neuron previousLayerNeuron){
		for(Neuron each: neurons)
			previousLayerNeuron.connect(each);
	}
	
	public NeuralLayer previous(){
		return previous;
	}
	
	public NeuralLayer next(){
		return next;
	}
	
	public String toString(){
		String result = name;
		for(Neuron each: neurons)
			result += each.toString();
		return result;
	}
}
