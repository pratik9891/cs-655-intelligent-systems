package sdsu.cs657.neuralNetwork;

import java.util.ArrayList;

public class TestInput {
	
	private Double input = 0.0;
	
	public TestInput(Double value){
		input = value;
	}
	
//	public void generate(){
//		for(double i =0; i<5;i=i+1)
//			inputs.add(i);
//	}
	
	public Double get(){
		return input;
	}

//	public int size(){
//		return inputs.size();
//	}
}
