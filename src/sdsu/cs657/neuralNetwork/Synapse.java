package sdsu.cs657.neuralNetwork;

public class Synapse {
	
	private double weight = ((Math.random() * 2) - 1);
	private double previousWeightCorrection = 0.0;
	
	private Neuron from;
	private Neuron to;
	
	public Synapse(Neuron from, Neuron to){
		this.from = from;
		this.to = to;
	}
	
	public double output(){
		return weight * from.output();
	}
	
	public double getWeight(){
		return weight;
	}
	
	public double getInput(){
		return from.output();
	}
	
	public Neuron from(){
		return from;
	}
	
	public Neuron to(){
		return to;
	}
	
	public void propogate(){
		double currentWeightCorrections = Constants.TRAINING_RATE * from.output() * to.errorGradient();
		this.weight += currentWeightCorrections + Constants.MOMENTUM * previousWeightCorrection;
		this.previousWeightCorrection = currentWeightCorrections;
	}
	
	public String toString(){
		return "weight: "+this.getWeight()+ " Input: "+this.getInput() + " Output:"+this.output()+"\n";
	}
	

}
