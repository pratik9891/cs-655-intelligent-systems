package sdsu.cs657.pathFinderGUI;

import java.util.Comparator;


//Helper class for sorting Cell collections

public class CellComparatorByDist implements Comparator<Cell>{ 
    @Override
    public int compare(Cell cell1, Cell cell2){ 
        return cell1.totalCost-cell2.totalCost; 
    } 
}
