package sdsu.cs657.pathFinderGUI;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;

public class Game {

	public static JFrame gameFrame;

	public void setup() {
		int windowWidth = 800;
		int windowHeight = 600;

		gameFrame = new JFrame("Autonomous path solver");
		gameFrame.setContentPane(new GamePanel(windowWidth, windowHeight));
		gameFrame.pack();
		gameFrame.setResizable(false);

		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		double screenWidth = screenSize.getWidth();
		double screenHeight = screenSize.getHeight();

		// To place the window equidistant from all sides of the screen
		int x = (int) (screenWidth - windowWidth) / 2;
		int y = (int) (screenHeight - windowHeight) / 2;
		gameFrame.setLocation(x, y);
		gameFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		gameFrame.setVisible(true);
	}

	public static void main(String[] args) {
		Game g = new Game();
		g.setup();
	}
}
