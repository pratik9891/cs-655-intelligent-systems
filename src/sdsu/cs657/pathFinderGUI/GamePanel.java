package sdsu.cs657.pathFinderGUI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.AffineTransform;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class GamePanel extends JPanel {

	private static final long serialVersionUID = 1L;

	private class MouseHandler implements MouseListener, MouseMotionListener {
		private int cur_row, cur_col, cur_val;

		@Override
		public void mousePressed(MouseEvent evt) {
			int row = (evt.getY() - 10) / squareSize;
			int col = (evt.getX() - 10) / squareSize;
			if (row >= 0 && row < rows && col >= 0 && col < columns
					&& !searching) {
				cur_row = row;
				cur_col = col;
				cur_val = gameMap.getCell(row, col).type;
				if (cur_val == Cell.EMPTY) {
					gameMap.fillCell(row, col, Cell.OBSTACLE);
				}
				if (cur_val == Cell.OBSTACLE) {
					gameMap.fillCell(row, col, Cell.EMPTY);
				}
			}
			repaint();
		}

		@Override
		public void mouseDragged(MouseEvent evt) {
			int row = (evt.getY() - 10) / squareSize;
			int col = (evt.getX() - 10) / squareSize;
			if (row >= 0 && row < rows && col >= 0 && col < columns
					&& !searching) {
				if ((row * columns + col != cur_row * columns + cur_col)
						&& (cur_val == Cell.ROVER || cur_val == Cell.TARGET)) {
					int new_val = gameMap.getCell(row, col).type;
					if (new_val == Cell.EMPTY) {
						gameMap.fillCell(row, col, cur_val);
						if (cur_val == Cell.ROVER) {
							roverPosition.row = row;
							roverPosition.column = col;
						} else {
							targetPosition.row = row;
							targetPosition.column = col;
						}
						gameMap.fillCell(cur_row, cur_col, new_val);
						cur_row = row;
						cur_col = col;
						if (cur_val == Cell.ROVER) {
							roverPosition.row = cur_row;
							roverPosition.column = cur_col;
						} else {
							targetPosition.row = cur_row;
							targetPosition.column = cur_col;
						}
						cur_val = gameMap.getCell(row, col).type;
					}
				} else if (gameMap.getCell(row, col).type != Cell.ROVER
						&& gameMap.getCell(row, col).type != Cell.TARGET) {
					gameMap.fillCell(row, col, Cell.OBSTACLE);
				}
			}
			repaint();
		}

		@Override
		public void mouseReleased(MouseEvent evt) {
		}

		@Override
		public void mouseEntered(MouseEvent evt) {
		}

		@Override
		public void mouseExited(MouseEvent evt) {
		}

		@Override
		public void mouseMoved(MouseEvent evt) {
		}

		@Override
		public void mouseClicked(MouseEvent evt) {
		}

	}

	int rows = 25, columns = 25, squareSize = 500 / rows;

	int arrowSize = squareSize / 2;

	Cell roverPosition;
	Cell targetPosition;

	JLabel messageLabel;

	JTextField rowsField, columnsField, generateField, directionField;
	JButton resetButton, updateButton, runButton, stepButton, allButton,
			generateButton, directionButton, saveButton;

	Map gameMap, oldGameMap;
	Rover r2d2;
	InferenceEngine kb;

	int direction;

	boolean searching, pathPlotting;

	GamePanel(int width, int height) {
		setLayout(null);
		MouseHandler mouse = new MouseHandler();
		addMouseListener(mouse);
		addMouseMotionListener(mouse);

		setPreferredSize(new Dimension(width, height));

		gameMap = new Map(rows, columns);
		roverPosition = gameMap.getCell(0, 0);
		targetPosition = gameMap.getCell(rows - 1, columns - 1);

		JLabel rowsLbl = new JLabel("Number of rows:", JLabel.RIGHT);
		rowsLbl.setFont(new Font("Helvetica", Font.PLAIN, 13));
		rowsField = new JTextField();
		rowsField.setColumns(3);
		rowsField.setText(Integer.toString(rows));

		JLabel columnsLbl = new JLabel("Number of columns:", JLabel.RIGHT);
		columnsLbl.setFont(new Font("Helvetica", Font.PLAIN, 13));
		columnsField = new JTextField();
		columnsField.setText(Integer.toString(columns));

		JLabel generateLbl = new JLabel("Percentage occuppancy", JLabel.RIGHT);
		generateLbl.setFont(new Font("Helvetica", Font.PLAIN, 13));
		generateField = new JTextField();
		generateField.setText(Integer.toString(columns));

		JLabel directionLbl = new JLabel("Rover initial direction",
				JLabel.RIGHT);
		directionLbl.setFont(new Font("Helvetica", Font.PLAIN, 13));
		directionField = new JTextField();

		resetButton = new JButton("Reset");
		resetButton.setBackground(Color.lightGray);
		resetButton.setToolTipText("Resets a Game");
		resetButton.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {

				resetButtonActionPerformed(evt);
			}
		});

		updateButton = new JButton("Clear/Update");
		updateButton.setBackground(Color.lightGray);
		updateButton
				.setToolTipText("Updates grid size and initializes a new game");
		updateButton.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				updateButtonActionPerformed(evt);
			}
		});

		generateButton = new JButton("Generate");
		generateButton.setBackground(Color.lightGray);
		generateButton.setToolTipText("Resets a Game");
		generateButton.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {

				generateButtonActionPerformed(evt);
			}
		});

		runButton = new JButton("Run");
		runButton.setBackground(Color.lightGray);
		runButton
				.setToolTipText("Locks the grid for any changes and applies the expert system");
		runButton.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				runButtonActionPerformed(evt);
			}
		});

		stepButton = new JButton("Step");
		stepButton.setBackground(Color.lightGray);
		stepButton.setToolTipText("Step by Step animation");
		stepButton.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				stepButtonActionPerformed(evt);
			}
		});

		allButton = new JButton("All at once");
		allButton.setBackground(Color.lightGray);
		allButton.setToolTipText("all at once with no animation");
		allButton.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				allButtonActionPerformed(evt);
			}
		});

		directionButton = new JButton("Set Direction");
		directionButton.setBackground(Color.lightGray);
		directionButton.setToolTipText("Resets a Game");
		directionButton.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {

				directionButtonActionPerformed(evt);
			}
		});
		
		saveButton = new JButton("Save LOG");
		saveButton.setBackground(Color.lightGray);
		saveButton.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				saveButtonActionPerformed(evt);
			}
		});

		messageLabel = new JLabel("1)Generate/Draw Map\n2)press RUN", JLabel.CENTER);
		messageLabel.setFont(new Font("Helvetica", Font.BOLD, 20));

		add(messageLabel);
		add(rowsLbl);
		add(rowsField);
		add(columnsLbl);
		add(columnsField);
		add(resetButton);
		add(updateButton);
		add(runButton);
		add(stepButton);
		add(allButton);
		add(generateButton);
		add(generateField);
		add(generateLbl);
		add(directionLbl);
		add(directionButton);
		add(directionField);
		add(saveButton);

		messageLabel.setBounds(50, 520, 500, 30);
		rowsLbl.setBounds(520, 5, 140, 25);
		rowsField.setBounds(665, 5, 35, 25);
		columnsLbl.setBounds(520, 35, 140, 25);
		columnsField.setBounds(665, 35, 35, 25);
		resetButton.setBounds(520, 95, 170, 25);
		updateButton.setBounds(520, 65, 170, 25);
		runButton.setBounds(520, 125, 170, 25);
		stepButton.setBounds(520, 155, 170, 25);
		allButton.setBounds(520, 185, 170, 25);
		generateLbl.setBounds(520, 220, 140, 25);
		generateField.setBounds(665, 220, 35, 25);
		generateButton.setBounds(520, 255, 170, 25);
		saveButton.setBounds(520, 350, 170, 25);

		directionLbl.setBounds(520, 280, 140, 25);
		directionField.setBounds(665, 280, 50, 25);
		directionButton.setBounds(520, 315, 170, 25);

		stepButton.setEnabled(false);
		allButton.setEnabled(false);
		saveButton.setEnabled(false);

		fillGrid();
	}

	private void directionButtonActionPerformed(java.awt.event.ActionEvent evt) {
		String d = directionField.getText().toString();
		d = d.replaceAll("\\s", "");
		d = d.replaceAll("(?!\")\\p{Punct}", "");
		d = d.toLowerCase();
		if (d.equals("south"))
			direction = 0;
		if (d.equals("southwest"))
			direction = 1;
		if (d.equals("west"))
			direction = 2;
		if (d.equals("northwest"))
			direction = 3;
		if (d.equals("north"))
			direction = 4;
		if (d.equals("northeast"))
			direction = 5;
		if (d.equals("east"))
			direction = 6;
		if (d.equals("southeast"))
			direction = 7;
	}

	private void fillGrid() {
		gameMap.fillCell(targetPosition.row, targetPosition.column, Cell.TARGET);
		gameMap.fillCell(roverPosition.row, roverPosition.column, Cell.ROVER);
		repaint();
	}

	private void resetButtonActionPerformed(java.awt.event.ActionEvent evt) {
		messageLabel.setText("1)Generate/Draw Map\n2)press RUN");
		gameMap = oldGameMap;
		searching = false;
		repaint();
	}

	private void updateButtonActionPerformed(java.awt.event.ActionEvent evt) {
		messageLabel.setText("1)Generate/Draw Map\n2)press RUN");
		rows = Integer.parseInt(rowsField.getText());
		columns = Integer.parseInt(columnsField.getText());
		initializeGrid();
	}

	private void generateButtonActionPerformed(java.awt.event.ActionEvent evt) {
		float percentage = Float.parseFloat(generateField.getText());
		int numberOfObstacles = (int) (percentage / 100 * rows * columns);
		for (int i = 0; i < numberOfObstacles; i++) {
			gameMap.fillCell((int) (Math.random() * rows),
					(int) (Math.random() * columns), Cell.OBSTACLE);
		}
		messageLabel.setText(percentage + "% Random obstacles generated!");
		fillGrid();
	}

	private void runButtonActionPerformed(java.awt.event.ActionEvent evt) {
		resetButton.setEnabled(false);
		updateButton.setEnabled(false);
		searching = true;
		String text = (String) evt.getActionCommand();
		if (text.equals("Run")) {
			messageLabel.setText("Running..");
			stepButton.setEnabled(true);
			allButton.setEnabled(true);
			generateButton.setEnabled(false);
			runButton.setText("Stop");
			saveButton.setEnabled(true);
			runAlgorithm();
		} else if (text.equals("Stop")) {
			messageLabel.setText("Stopped! Please press clear/reset.");
			stepButton.setEnabled(false);
			allButton.setEnabled(false);
			resetButton.setEnabled(true);
			updateButton.setEnabled(true);
			generateButton.setEnabled(true);
			searching = false;
			runButton.setText("Run");
			saveButton.setEnabled(false);
		}

	}

	private void allButtonActionPerformed(java.awt.event.ActionEvent evt) {
		int count = 0;
		while (kb.processRules() != 1 && count < rows * columns * 2) {
			count++;
		}
		int moves = r2d2.currentRoute.getLength() + r2d2.numberOfMoves;

		if (count > rows * columns * 1.5f)
			messageLabel.setText("Rover is stuck, please press STOP");
		else
			messageLabel.setText("number of moves: " + moves + " (Press Stop)");
		repaint();

	}
	
	private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {
		writeOutput();
	}
	
	public void writeOutput(){
		try {
			FileWriter file = new FileWriter("output"+".txt");
	        PrintWriter output = new PrintWriter(file);
	        output.println(r2d2.facts.realMap.toString()); 
	        output.println(roverPosition.toString() + " "+ direction);
	        output.print(r2d2.currentRoute.toString());
	        output.print(targetPosition.toString());
	        output.flush();
	        output.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void stepButtonActionPerformed(java.awt.event.ActionEvent evt) {
		int moves = 0;
		int x = kb.processRules();
		if (x == -1) {
			moves = r2d2.currentRoute.getLength() + r2d2.numberOfMoves - 1;
			if (moves > 2 * rows * columns) {
				messageLabel.setText("Rover is stuck, please press STOP");
				return;
			}
			messageLabel.setText("Current moves count: " + moves);
		}
		if (x == 1) {
			moves = r2d2.currentRoute.getLength() + r2d2.numberOfMoves;
			messageLabel.setText("Current moves count: " + moves
					+ " (Press Stop)");
			searching = false;
		}
		repaint();
	}

	private void runAlgorithm() {
		r2d2 = new Rover(gameMap, roverPosition, targetPosition, direction);
		kb = new InferenceEngine(r2d2);
		oldGameMap = gameMap;
		gameMap = r2d2.facts.roverMap;
		repaint();
		//For degbugging purposes
		// while(kb.processRules() != 1){}
		// System.out.print("test");
	}

	private void initializeGrid() {
		squareSize = 500 / (rows > columns ? rows : columns);
		arrowSize = squareSize / 2;
		gameMap = new Map(rows, columns);
		roverPosition = new Cell(0, 0);
		targetPosition = new Cell(rows - 1, columns - 1);
		fillGrid();
	}

	public void paintComponent(Graphics g) {

		super.paintComponent(g);

		g.setColor(Color.DARK_GRAY);
		g.fillRect(10, 10, columns * squareSize + 1, rows * squareSize + 1);

		g.setColor(Color.red);
		g.fillRect(550, 400, 20, 20);
		g.drawString("Rover", 575, 410);
		g.setColor(Color.green);
		g.fillRect(550, 415, 20, 20);
		g.drawString("Goal", 575, 425);
		g.setColor(Color.white);
		g.fillRect(550, 430, 20, 20);
		g.drawString("Vacant spot", 575, 440);
		g.setColor(Color.LIGHT_GRAY);
		g.fillRect(550, 445, 20, 20);
		g.drawString("Unknown spot", 575, 455);
		g.setColor(Color.BLACK);
		g.fillRect(550, 460, 20, 20);
		g.drawString("Obstacle", 575, 470);

		for (int r = 0; r < rows; r++) {
			for (int c = 0; c < columns; c++) {

				if (gameMap.getCell(r, c).type == Cell.EMPTY) {
					g.setColor(Color.WHITE);
				} else if (gameMap.getCell(r, c).type == Cell.ROVER) {
					g.setColor(Color.RED);
				} else if (gameMap.getCell(r, c).type == Cell.TARGET) {
					g.setColor(Color.GREEN);
				} else if (gameMap.getCell(r, c).type == Cell.OBSTACLE) {
					g.setColor(Color.BLACK);
				} else if (gameMap.getCell(r, c).type == Cell.UNKNOWN) {
					g.setColor(Color.LIGHT_GRAY);
				}
				g.fillRect(11 + c * squareSize, 11 + r * squareSize,
						squareSize - 1, squareSize - 1);
			}
		}
		if (searching) {
			g.setColor(Color.blue);
			for (int i = 0; i < r2d2.currentRoute.getLength(); i++) {
				if (i >= 1) {
					drawArrow(g, 11 + squareSize
							* r2d2.currentRoute.getStep(i).column + squareSize
							/ 2,
							11 + squareSize * r2d2.currentRoute.getStep(i).row
									+ squareSize / 2, 11 + squareSize
									* r2d2.currentRoute.getStep(i - 1).column
									+ squareSize / 2, 11 + squareSize
									* r2d2.currentRoute.getStep(i - 1).row
									+ squareSize / 2);
				}
			}
		}

	}

	private void drawArrow(Graphics g1, int x1, int y1, int x2, int y2) {
		Graphics2D g = (Graphics2D) g1.create();

		double dx = x2 - x1, dy = y2 - y1;
		double angle = Math.atan2(dy, dx);
		int len = (int) Math.sqrt(dx * dx + dy * dy);
		AffineTransform at = AffineTransform.getTranslateInstance(x1, y1);
		at.concatenate(AffineTransform.getRotateInstance(angle));
		g.transform(at);

		g.drawLine(0, 0, len, 0);
		g.drawLine(0, 0, (int) (arrowSize * Math.sin(70 * Math.PI / 180)),
				(int) (arrowSize * Math.cos(70 * Math.PI / 180)));
		g.drawLine(0, 0, (int) (arrowSize * Math.sin(70 * Math.PI / 180)),
				-(int) (arrowSize * Math.cos(70 * Math.PI / 180)));

	}

}
