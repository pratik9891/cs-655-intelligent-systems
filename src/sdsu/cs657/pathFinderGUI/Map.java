package sdsu.cs657.pathFinderGUI;

import java.util.ArrayList;

public class Map {

	private int rows;
	private int columns;
	private Cell[][] map;

	public Map(int inputRows, int inputCols) {
		rows = inputRows;
		columns = inputCols;
		map = new Cell[rows][columns];
		for (int x = 0; x < rows; x++) {
			for (int y = 0; y < columns; y++) {
				map[x][y] = new Cell(x, y);
			}
		}
	}

	public void fillCell(int x, int y, int type) {
		map[x][y].type = type;
	}

	public int getRows() {
		return rows;
	}

	public int getColumns() {
		return columns;
	}

	public void reset() {
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				map[i][j].type = Cell.EMPTY;
				map[i][j].setAsNotVisited();
			}
		}
	}

	public Cell getCell(int row, int column) {
		return map[row][column];
	}

	public ArrayList<Cell> getNeighbours(int row, int column) {
		ArrayList<Cell> result = new ArrayList<Cell>();

		// Efficient way of finding neighbours
		// http://ubuntuforums.org/showthread.php?t=854818

		int minSubx = ((column - 1) < 0) ? 0 : column - 1;
		int minSuby = ((row - 1) < 0) ? 0 : row - 1;
		int maxSubx = ((column + 1) >= columns) ? columns - 1 : column + 1;
		int maxSuby = ((row + 1) >= rows) ? rows - 1 : row + 1;

		for (int px = minSubx; px <= maxSubx; px++) {
			for (int py = minSuby; py <= maxSuby; py++) {
				if (!(px == column && py == row))
					result.add(map[py][px]);
			}
		}
		return result;
	}
	
	public String toString(){
		return rows + " x " + columns;
	}
}
