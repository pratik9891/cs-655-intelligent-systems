package sdsu.cs657.pathFinderGUI;

public class Rover {

	private int direction = Database.SOUTH;

	public Database facts;
	private Cell selfPosition;
	public int numberOfMoves = 0;
	public Route currentRoute = new Route();

	Rover(Map real, Cell startPosition, Cell endPosition, int initialDirection) {
		direction = initialDirection;
		selfPosition = startPosition;
		facts = new Database(real, startPosition, endPosition);
		facts.updateRoverMap(direction, selfPosition);
		facts.updateNeighbours();
		currentRoute.appendStep(startPosition);
	}

	public void changeDirectionTowards(Cell newCell) {
		int neighbourDirection = 0;
		int dx = newCell.column - selfPosition.column;
		int dy = newCell.row - selfPosition.row;
		if (dx == 0 && dy >= 1) // SOUTH
			neighbourDirection = 0;
		if (dx <= -1 && dy >= 1) // SOUTHWEST
			neighbourDirection = 1;
		if (dx <= -1 && dy == 0) // WEST
			neighbourDirection = 2;
		if (dx <= -1 && dy <= -1) // Northwest
			neighbourDirection = 3;
		if (dx == 0 && dy <= -1) // North
			neighbourDirection = 4;
		if (dx >= 1 && dy <= -1) // NORThEAST
			neighbourDirection = 5;
		if (dx >= 1 && dy == 0) // East
			neighbourDirection = 6;
		if (dx >= 1 && dy >= 1) // SouthEast
			neighbourDirection = 7;

		int oneWay = ((direction - neighbourDirection) % 8 < 0) ? direction
				- neighbourDirection + 8 : direction - neighbourDirection;
		numberOfMoves += Math.min(oneWay, 8 - oneWay);
		direction = neighbourDirection;
		facts.updateRoverMap(direction, selfPosition);
		facts.updateRoverPosition(selfPosition);
	}

	public void move(Cell tempDestination) {

		currentRoute.appendStep(tempDestination);
		facts.roverMap.fillCell(selfPosition.row, selfPosition.column,
				Cell.EMPTY);
		selfPosition = tempDestination;
		facts.updateRoverMap(direction, selfPosition);
		facts.updateRoverPosition(selfPosition);

	}

}
