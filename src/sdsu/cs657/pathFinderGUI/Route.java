package sdsu.cs657.pathFinderGUI;

import java.util.ArrayList;

public class Route {

	private ArrayList<Cell> steps = new ArrayList<Cell>();

	public int getLength() {
		return steps.size();
	}

	public Cell getStep(int index) {
		return (Cell) steps.get(index);
	}

	public void appendStep(Cell step) {
		steps.add(step);
	}
	
	public String toString(){
		String result = "";
		for(Cell each: steps){
			result += each.toString() + "\n";
		}
		return result;
	}

}
