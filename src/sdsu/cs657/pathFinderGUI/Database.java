package sdsu.cs657.pathFinderGUI;

import java.util.ArrayList;
import java.util.Collections;


//Store the facts for rover
public class Database {

	public static final int SOUTH = 0, SOUTHWEST = 1, WEST = 2, NORTHWEST = 3,
			NORTH = 4, NORTHEAST = 5, EAST = 6, SOUTHEAST = 7;

	public Map roverMap;
	public Map realMap;
	public Cell targetPosition;
	private Cell roverPosition;
	public ArrayList<Cell> neighbours = new ArrayList<Cell>(8);
	private int roverDirection;

	Database(Map real, Cell startPosition, Cell endPosition) {
		realMap = real;
		roverMap = new Map(real.getRows(), real.getColumns());
		roverPosition = startPosition;
		targetPosition = endPosition;
		for (int i = 0; i < realMap.getRows(); i++) {
			for (int j = 0; j < realMap.getColumns(); j++) {
				int tempType = realMap.getCell(i, j).type;
				roverMap.getCell(i, j).distanceFrom(targetPosition);
				if (tempType != Cell.ROVER && tempType != Cell.TARGET) {
					roverMap.fillCell(i, j, Cell.UNKNOWN);
				}
			}
		}
		roverMap.fillCell(targetPosition.row, targetPosition.column,
				Cell.TARGET);
		roverMap.fillCell(roverPosition.row, roverPosition.column, Cell.ROVER);
		roverPosition.setAsVisited();
	}

	public boolean remainingAreVisited() {
		boolean result = true;
		for (Cell each : neighbours) {
			if (!(each.isVisited()))
				return false;
		}
		return result;
	}

	public void updateRoverMap(int roverD, Cell roverLocation) {
		roverDirection = roverD;

		int leftDirection = ((((roverDirection - 1) % 8) < 0) ? roverDirection - 1 + 8
				: roverDirection - 1);
		int rightDirection = ((roverDirection + 1) % 8);
		updateView(roverDirection, roverLocation);
		updateView(leftDirection, roverLocation);
		updateView(rightDirection, roverLocation);
	}

	public void updateRoverPosition(Cell newPostion) {
		roverPosition = newPostion;
		roverMap.fillCell(newPostion.row, newPostion.column, Cell.ROVER);
		roverPosition.setAsVisited();
		updateNeighbours();
	}

	public void updateNeighbours() {
		neighbours.clear();
		neighbours = roverMap.getNeighbours(roverPosition.row,
				roverPosition.column);
		totalNeighbourCost();
	}

	public Cell getNextBestPossibleMove() {
		return neighbours.get(0);
	}

	public int numberOfMovesToChangeDirectionToNeighbour(Cell destination) {
		int neighboursLocation = 0;
		int dx = destination.column - roverPosition.column;
		int dy = destination.row - roverPosition.row;
		if (dx == 0 && dy >= 1) // SOUTH
			neighboursLocation = 0;
		if (dx <= -1 && dy >= 1) // SOUTHWEST
			neighboursLocation = 1;
		if (dx <= -1 && dy == 0) // WEST
			neighboursLocation = 2;
		if (dx <= -1 && dy <= -1) // Northwest
			neighboursLocation = 3;
		if (dx == 0 && dy <= -1) // North
			neighboursLocation = 4;
		if (dx >= 1 && dy <= -1) // NORThEAST
			neighboursLocation = 5;
		if (dx >= 1 && dy == 0) // East
			neighboursLocation = 6;
		if (dx >= 1 && dy >= 1) // SouthEast
			neighboursLocation = 7;

		int oneWay = ((roverDirection - neighboursLocation) % 8 < 0) ? roverDirection
				- neighboursLocation + 8
				: roverDirection - neighboursLocation;
		return Math.min(oneWay, 8 - oneWay);
	}

	public void totalNeighbourCost() {
		for (Cell each : neighbours) {
			each.totalCost = each.distance
					+ numberOfMovesToChangeDirectionToNeighbour(each);
		}
		sort();
	}

	public void sort() {
		Collections.sort(neighbours, new CellComparatorByDist());
	}

	public void removeNeighbour(Cell c) {
		neighbours.remove(c);
	}

	private void updateView(int direction, Cell currentLocation) {
		Cell self = currentLocation;
		if (direction == Database.SOUTH) {
			for (int row = self.row; row < roverMap.getRows(); row++) {
				int temp = realMap.getCell(row, self.column).type;
				if (temp == Cell.EMPTY)
					roverMap.fillCell(row, self.column, Cell.EMPTY);
				if (temp == Cell.OBSTACLE) {
					roverMap.fillCell(row, self.column, Cell.OBSTACLE);
					return;
				}
			}
		}
		if (direction == Database.SOUTHWEST) {
			for (int row = self.row, column = self.column; column >= 0
					&& row < roverMap.getRows(); row++, column--) {
				int temp = realMap.getCell(row, column).type;
				if (temp == Cell.EMPTY)
					roverMap.fillCell(row, column, Cell.EMPTY);
				if (temp == Cell.OBSTACLE) {
					roverMap.fillCell(row, column, Cell.OBSTACLE);
					return;
				}
			}
		}
		if (direction == Database.WEST) {
			for (int column = self.column; column >= 0; column--) {
				int temp = realMap.getCell(self.row, column).type;
				if (temp == Cell.EMPTY)
					roverMap.fillCell(self.row, column, Cell.EMPTY);
				if (temp == Cell.OBSTACLE) {
					roverMap.fillCell(self.row, column, Cell.OBSTACLE);
					return;
				}
			}
		}
		if (direction == Database.NORTHWEST) {
			for (int row = self.row, column = self.column; row >= 0
					&& column >= 0; row--, column--) {
				int temp = realMap.getCell(row, column).type;
				if (temp == Cell.EMPTY)
					roverMap.fillCell(row, column, Cell.EMPTY);
				if (temp == Cell.OBSTACLE) {
					roverMap.fillCell(row, column, Cell.OBSTACLE);
					return;
				}
			}
		}
		if (direction == Database.NORTH) {
			for (int row = self.row; row >= 0; row--) {
				int temp = realMap.getCell(row, self.column).type;
				if (temp == Cell.EMPTY)
					roverMap.fillCell(row, self.column, Cell.EMPTY);
				if (temp == Cell.OBSTACLE) {
					roverMap.fillCell(row, self.column, Cell.OBSTACLE);
					return;
				}
			}
		}
		if (direction == Database.NORTHEAST) {
			for (int row = self.row, column = self.column; column < roverMap
					.getColumns() && row >= 0; row--, column++) {
				int temp = realMap.getCell(row, column).type;
				if (temp == Cell.EMPTY)
					roverMap.fillCell(row, column, Cell.EMPTY);
				if (temp == Cell.OBSTACLE) {
					roverMap.fillCell(row, column, Cell.OBSTACLE);
					return;
				}
			}
		}
		if (direction == Database.EAST) {
			for (int column = self.column; column < roverMap.getColumns(); column++) {
				int temp = realMap.getCell(self.row, column).type;
				if (temp == Cell.EMPTY) {
					roverMap.fillCell(self.row, column, Cell.EMPTY);
				}
				if (temp == Cell.OBSTACLE) {
					roverMap.fillCell(self.row, column, Cell.OBSTACLE);
					return;
				}
			}
		}
		if (direction == Database.SOUTHEAST) {
			for (int row = self.row, column = self.column; column < roverMap
					.getColumns() && row < roverMap.getRows(); row++, column++) {
				int temp = realMap.getCell(row, column).type;
				if (temp == Cell.EMPTY)
					roverMap.fillCell(row, column, Cell.EMPTY);
				if (temp == Cell.OBSTACLE) {
					roverMap.fillCell(row, column, Cell.OBSTACLE);
					return;
				}
			}
		}

	}
}
