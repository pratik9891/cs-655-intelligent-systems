package sdsu.cs657.pathFinderGUI;

//The building block for a cell

public class Cell {
	public int row;
	public int column;

	public int totalCost;
	public int distance;
	public int type;
	private boolean visited = false;

	public static final int 
		EMPTY = 0, 
		OBSTACLE = 1, 
		ROVER = 2, 
		TARGET = 3,
		UNKNOWN = 4;

	public Cell(int x, int y) {
		this.row = x;
		this.column = y;
	}

	public void distanceFrom(Cell destination) {
		int dx = destination.row - this.row;
		int dy = destination.column - this.column;
		this.distance = (int) ((double) 1000 * Math.sqrt(dx * dx + dy * dy));
	}

	public void setType(int type) {
		this.type = type;
	}

	public void setAsVisited() {
		this.visited = true;
	}

	public boolean isVisited() {
		return visited;
	}
	
	public void setAsNotVisited(){
		visited = false;
	}
	
	public int getTotalCost(){
		return totalCost;
	}
	
	public void setTotalCost(int newCost){
		totalCost = newCost;
	}
	
	public String toString(){
		return "(" + row + "," + column + ")";
	}

}
