package sdsu.cs657.pathFinderGUI;

public class InferenceEngine {

	Database facts;
	Rover r2d2;
	Cell roverLocation;
	Cell targetPosition;

	public InferenceEngine(Rover r) {
		r2d2 = r;
		facts = r2d2.facts;
		targetPosition = facts.targetPosition;
	}

	public int processRules() {
		Cell nextMove;
		try {
			nextMove = facts.getNextBestPossibleMove();
		} catch (IndexOutOfBoundsException e) {
			return 0;
		}
		if (nextMove.row == targetPosition.row
				&& nextMove.column == targetPosition.column)
			return 1;
		if (nextMove.type == Cell.OBSTACLE) {
			facts.removeNeighbour(nextMove);
			return -1;
		}
		if (nextMove.isVisited() && (facts.remainingAreVisited())) {
			r2d2.move(nextMove);
			return -1;
		}
//		if (nextMove.isVisited()) {
//			nextMove.totalCost += Math.random()
//					* (facts.roverMap.getRows() * facts.roverMap.getRows() + facts.roverMap
//							.getColumns() * facts.roverMap.getColumns())
//					* 10000;
//			facts.sort();
//			return -1;
//		}
		if (nextMove.isVisited()) {
			nextMove.totalCost +=  10000;
			facts.sort();
			return -1;
		}
		if (nextMove.type == Cell.EMPTY && !(nextMove.isVisited())) {
			r2d2.changeDirectionTowards(nextMove);
			r2d2.move(nextMove);
			return -1;
		}
		if (nextMove.type == Cell.UNKNOWN) {
			r2d2.changeDirectionTowards(nextMove);
			return -1;
		}

		return -1;

	}

}