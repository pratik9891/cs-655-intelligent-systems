package sdsu.cs657.stockTrading;

import java.util.ArrayList;
import java.util.List;

public class FuzzyEngine {
	
	List<FuzzyRule> rules;
	List<LinguisticVariable> variables;
	Defuzzify defuz;
	
	public FuzzyEngine() {
		rules = new ArrayList<FuzzyRule>();
		variables = new ArrayList<LinguisticVariable>();
		defuz = new Defuzzify();
	}
	
	public void addVariable(LinguisticVariable lv) {
	        variables.add(lv);
	}
	
	public void addRule(FuzzyTerm ant, FuzzyTerm con) {
	        rules.add(new FuzzyRule(ant, con));
	}
	
	public void fuzzify(String label, double val) {
        for (LinguisticVariable lv : variables) {
            if (label.equalsIgnoreCase(lv.getLabel())) {
                lv.fuzziffy(val);
            }
        }
    }
	
	public double defuzzify(String label) throws Exception {
        resetConsequents();
        LinguisticVariable lv = null;
        for (LinguisticVariable l : variables) {
            if (l.getLabel().equalsIgnoreCase(label)) {
                lv = l;
            }
        }

        if (lv == null) {
            throw new Exception("Ligusitic variable not found");
        } else {
            for (FuzzyRule r : rules) {
                r.calculate();
            }
            return defuz.getDefuzziedValue(lv);
        }
    }
	
	private void resetConsequents(){
		for (FuzzyRule r : rules) 
            r.getConsequent().clearDegreeOfMembership();
	}

}
