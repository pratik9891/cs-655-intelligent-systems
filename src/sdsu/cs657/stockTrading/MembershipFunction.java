package sdsu.cs657.stockTrading;

import java.util.ArrayList;

public class MembershipFunction {

    private double a;
    private double b;
    private double c;
    private double d;

    public MembershipFunction(double a, double b, double c, double d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }


    public double getValue(double input) {
        double result = 0;
        if (input >= a && input <= b) {
            result = (input - a) / (b - a);
            return result;
        }
        if (input >= b && input <= c) {
            result = 1;
            return result;
        }
        if (input >= c && input <= d) {
            result = (input - d) / (c - d);
        }
        return result;
    }

    public double getClippedValue(double input, double clip) {
        double result = 0;
        double flag = 0;
        if (input >= a && input <= b) {
            flag = (input - a) / (b - a);
            if (flag < clip) {
                result = flag;
            } else {
                result = clip;
            }
        }
        if (input >= b && input <= c) {
            flag = 1;
            if (flag < clip) {
                result = flag;
            } else {
                result = clip;
            }
        }
        if (input >= c && input <= d) {
            flag = (input - d) / (c - d);
        }
        if (flag < clip) {
            result = flag;
        } else {
            result = clip;
        }
        return result;
 //   	return clip;
    }


    public double getMax() {
        if (Double.isInfinite(d)) {
            if (Double.isInfinite(c)) {
                if (Double.isInfinite(b)) {
                    return a;
                }
                return b;
            }
            return c;
        }
        return d;
    }



    public double getMin() {
        if (Double.isInfinite(a)) {
            if (Double.isInfinite(b)) {
                if (Double.isInfinite(c)) {
                    return d;
                }
                return c;
            }
            return b;
        }
        return a;
    }
    
    public ArrayList<Double> getPoints(){
    	ArrayList<Double> result = new ArrayList<Double>();
    	result.add(a);
    	result.add(b);
    	result.add(c);
    	result.add(d);
    	return result;
    }

}
