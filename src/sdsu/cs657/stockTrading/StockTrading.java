package sdsu.cs657.stockTrading;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Scanner;



public class StockTrading {

	public double cash = 10000;
	public double stocks = 0;
	public double totalAsset = 10000;

	public static void main(String[] args) throws Exception {
		
		
		Scanner reader = new Scanner(System.in);
		System.out.println("Enter number of trials");
		int input=reader.nextInt();
		System.out.println("Enter mode number [ 1 - normal or 2 - hack]:");
		int option = reader.nextInt();
		if( option != 1 && option != 2) {
			System.out.println("Next time choose eihter a 1 or a 2!");
			return;
		}
		StockTrading test = new StockTrading();
		File dir = new File("Logs");
		dir.mkdir();
		for(File file: dir.listFiles()) 
			file.delete(); 
		
		FileWriter file = new FileWriter("Result"+".txt");
		PrintWriter output = new PrintWriter(file);
		output.flush();
		output.close();
	}

	public double getNextRandom(int i) {
		double ri = Math.random() * 2 - 1;
		return (8 * ri * (i % 2));
	}

	public double getPrice(int i) {
		return 12 + 2.5 * Math.sin((2 * Math.PI * i) / 19) + 0.8
				* Math.cos((2 * Math.PI * i) / 5) + getNextRandom(i);
	}

	public double getMAD(int i) {
		return (0.5 * (Math.cos(0.3 * i))) - Math.sin(0.3 * i);
	}

	public double run(int j, int option) throws Exception {
		cash= 10000;
		stocks = 0;
		totalAsset = 0;
		
		FuzzyEngine fe = new FuzzyEngine();
		LinguisticVariable mad = new LinguisticVariable("mad");
		FuzzySet n = mad.addSet("n", new MembershipFunction(-1.30, -1.30, -0.8, 0));
		FuzzySet z = mad.addSet("z", new MembershipFunction(-0.8, 0, 0, 0.8));
		FuzzySet p = mad.addSet("p", new MembershipFunction(0, 0.8, 0.8, 1.30));
		fe.addVariable(mad);

		LinguisticVariable price = new LinguisticVariable("price");
		FuzzySet vl = price.addSet("vl", new MembershipFunction(0.85, 0.85, 4, 7.5));
		FuzzySet l = price.addSet("l", new MembershipFunction(4,7.5,7.5, 12));
		FuzzySet m = price.addSet("m", new MembershipFunction(7.5,12,12, 16));
		FuzzySet h = price.addSet("h", new MembershipFunction(12,16,16, 20.5));
		FuzzySet vh = price.addSet("vh",
				new MembershipFunction(16, 20.5, 23.5, 23.5));
		fe.addVariable(price);

		LinguisticVariable advise = new LinguisticVariable("advise");
		FuzzySet sm = advise.addSet("sm", new MembershipFunction(-700, -700, -500,-250));
		FuzzySet sf = advise.addSet("sf", new MembershipFunction(-500, -250,-250,0));
		FuzzySet dt = advise.addSet("dt", new MembershipFunction(-250,0,0, 250));
		FuzzySet bf = advise.addSet("bf", new MembershipFunction(0,250,250, 500));
		FuzzySet bm = advise.addSet("bm", new MembershipFunction(250, 500, 700, 700));
		fe.addVariable(advise);

		if ( option == 2){
		fe.addRule(FuzzyAnd.and(vl, n), bm);
		fe.addRule(FuzzyAnd.and(vl, z), bm);
		fe.addRule(FuzzyAnd.and(vl, p), bm);

		fe.addRule(FuzzyAnd.and(l, n), bm);
		fe.addRule(FuzzyAnd.and(l, z), bm);
		fe.addRule(FuzzyAnd.and(l, p), bm);

		fe.addRule(FuzzyAnd.and(m, n), sm);
		fe.addRule(FuzzyAnd.and(m, z), dt);
		fe.addRule(FuzzyAnd.and(m, p), bm);

		fe.addRule(FuzzyAnd.and(h, n), sm);
		fe.addRule(FuzzyAnd.and(h, z), sm);
		fe.addRule(FuzzyAnd.and(h, p), bm);

		fe.addRule(FuzzyAnd.and(vh, n), sm);
		fe.addRule(FuzzyAnd.and(vh, z), sm);
		fe.addRule(FuzzyAnd.and(vh, p), sm);
		} else {
			fe.addRule(FuzzyAnd.and(vl, n), sf);
			fe.addRule(FuzzyAnd.and(vl, z), bm);
			fe.addRule(FuzzyAnd.and(vl, p), bm);

			fe.addRule(FuzzyAnd.and(l, n), sf);
			fe.addRule(FuzzyAnd.and(l, z), bf);
			fe.addRule(FuzzyAnd.and(l, p), bm);

			fe.addRule(FuzzyAnd.and(m, n), sf);
			fe.addRule(FuzzyAnd.and(m, z), dt);
			fe.addRule(FuzzyAnd.and(m, p), bf);

			fe.addRule(FuzzyAnd.and(h, n), sm);
			fe.addRule(FuzzyAnd.and(h, z), sf);
			fe.addRule(FuzzyAnd.and(h, p), bf);

			fe.addRule(FuzzyAnd.and(vh, n), sm);
			fe.addRule(FuzzyAnd.and(vh, z), sm);
			fe.addRule(FuzzyAnd.and(vh, p), bf);
		}
		
		FileWriter file = new FileWriter("Logs/output-"+j+".txt");
		PrintWriter output = new PrintWriter(file);
		for (int i = 1; i < 151; i++) {
			
			output.println("Day: "+i);
			double todaysPrice = getPrice(i);
			double todaysMAD = getMAD(i);
			fe.fuzzify("mad", todaysMAD);
			fe.fuzzify("price", todaysPrice);
			double adv = (fe.defuzzify("advise"));
			output.println("MAD Value: "+todaysMAD);
			output.println("Stock price: "+todaysPrice);
			output.println("Advise: "+adv);
			if (adv < 0) {
				double actualTransaction = stocks > Math.abs(adv) ? Math.abs(adv) : stocks;
				stocks -= actualTransaction;
				cash += actualTransaction * todaysPrice;
				totalAsset = cash + (stocks * todaysPrice);
			} else {
				double currentBuyingCapacity = cash / todaysPrice;
				double actualTransaction = currentBuyingCapacity > adv ? adv
						: currentBuyingCapacity;
				stocks += actualTransaction;
				cash -= actualTransaction * todaysPrice;
				totalAsset = cash + (stocks * todaysPrice);
			}

			output.println("Cash: "+cash);
			output.println("Stocks: "+stocks);
			output.println("Total Assets: " + totalAsset);
			output.println("--------------------------------------");
		}
		output.flush();
	    output.close();
		System.out.println(j + ") Total assets for this test case: " + totalAsset);
		return totalAsset;
	}

}
