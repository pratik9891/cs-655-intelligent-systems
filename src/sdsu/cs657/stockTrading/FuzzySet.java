package sdsu.cs657.stockTrading;

public class FuzzySet implements FuzzyTerm {

    private String label;

    private MembershipFunction function;

    private double degree;

    public FuzzySet(String label, MembershipFunction function) {
        this.label = label;
        this.function = function;
        this.degree = 0;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof FuzzySet) {
            return getLabel().equalsIgnoreCase(obj.toString()) ? true : false;
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return getLabel();
    }

    public double getDegreeOfMembership() {
        return degree;
    }

    public void clearDegreeOfMembership() {
        degree = 0;
    }

    public void maxDegreeOfMembership(double val) {
        degree = Math.max(degree, val);
    }

    public double calculateDegreeOfMembership(double input) {
        degree = function.getValue(input);
      //  System.out.println(label+":"+degree);
        return degree;
    }

    
    public double getClippedValue(double input) {
        return function.getClippedValue(input, degree);
    }


    public String getLabel() {
        return label;
    }
    
    public MembershipFunction getMembershipFunction() {
        return function;
    }

    

}
