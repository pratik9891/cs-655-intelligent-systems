package sdsu.cs657.stockTrading;

public class FuzzyRule {

	private FuzzyTerm antecedent;
	private FuzzyTerm consequent;

	public FuzzyRule(FuzzyTerm antecedent, FuzzyTerm consequent) {
		this.antecedent = antecedent;
		this.consequent = consequent;
	}

	public void calculate() {
		consequent.maxDegreeOfMembership(getAntecedent().getDegreeOfMembership());
	}

	public FuzzyTerm getAntecedent() {
		return antecedent;
	}

	public FuzzyTerm getConsequent() {
		return consequent;
	}

}
