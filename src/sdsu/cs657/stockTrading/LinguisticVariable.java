package sdsu.cs657.stockTrading;

import java.util.ArrayList;
import java.util.List;


public class LinguisticVariable {

	private List<FuzzySet> setList;

	private String label;

	public LinguisticVariable(String label) {
		this.label = label;
		setList = new ArrayList<FuzzySet>();
	}


	public FuzzySet addSet(FuzzySet set) throws Exception {
		for (FuzzySet fs : setList) {
			if (set.equals(fs)) {
				throw new Exception("The label " + set.getLabel() + ""
						+ " is already registered");
			}
		}
		setList.add(set);
		return set;
	}


	public void fuzziffy(double input) {
		for (FuzzySet fs : setList) {
			fs.calculateDegreeOfMembership(input);
		}
	}


	public List<FuzzySet> getSetList() {
		return setList;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}


	public FuzzySet addSet(String setLabel,
			MembershipFunction MembershipFunction) throws Exception {
		FuzzySet set = new FuzzySet(setLabel, MembershipFunction);
		for (FuzzySet fs : setList) {
			if (set.equals(fs)) {
				throw new Exception("The label " + set.getLabel() + ""
						+ " is already registered");
			}
		}
		setList.add(set);
		return set;
	}

	public double getMax() {
		double flag = 0;
		double max = 0;
		for (FuzzySet fs : setList) {
			max = fs.getMembershipFunction().getMax();
			if (flag < max) {
				flag = max;
			}
		}
		return flag;
	}

	public double getMin() {
		double flag = 0;
		double min = 0;
		for (FuzzySet fs : setList) {
			min = fs.getMembershipFunction().getMin();
			if (flag > min) {
				flag = min;
			}
		}
		return flag;
	}
}
