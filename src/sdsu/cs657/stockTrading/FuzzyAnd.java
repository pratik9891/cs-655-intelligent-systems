package sdsu.cs657.stockTrading;

import java.util.ArrayList;
import java.util.List;

public class FuzzyAnd implements FuzzyTerm {

    List<FuzzyTerm> terms = new ArrayList<FuzzyTerm>();
    
    public static FuzzyTerm and(FuzzyTerm a, FuzzyTerm b) {
        return new FuzzyAnd(a,b);
    }

    public FuzzyAnd(FuzzyTerm... input) {
        for(FuzzyTerm eachTerm: input)
        	terms.add(eachTerm);    	
    }

    @Override
    public double getDegreeOfMembership() {
        double degreeOfMembership = Double.POSITIVE_INFINITY;
        for (FuzzyTerm t : terms) {
            if (t.getDegreeOfMembership() < degreeOfMembership) {
            	degreeOfMembership = t.getDegreeOfMembership();
            }
        }
        return degreeOfMembership;
    }

    @Override
    public void clearDegreeOfMembership() {
        for (FuzzyTerm t : terms) {
            t.clearDegreeOfMembership();
        }
    }

    @Override
    public void maxDegreeOfMembership(double val) {
        for (FuzzyTerm t : terms) {
            t.maxDegreeOfMembership(val);
        }
    }
}
