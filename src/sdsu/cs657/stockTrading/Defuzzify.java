package sdsu.cs657.stockTrading;

import java.util.List;

public class Defuzzify {

	List<FuzzySet> sets;
	double min;
	double max;
	int s;

	public Defuzzify() {
		sets = null;
		min = 0;
		max = 0;
		s = 100; //Larger value improves accuracy of defuzzification
	}

	public double getDefuzziedValue(LinguisticVariable lv) {
		sets = lv.getSetList();
		max = lv.getMax();
		
		min = lv.getMin();

		double jump = (max - min) / s;
		double sumAreas = 0;
		double sumCont = 0;

		double[] areas = new double[s];
		double[] values = new double[s];
		double flag = min + jump;
		for (int i = 0; i < s; i++) {
			for (FuzzySet fs : sets) {
				areas[i] += (fs.getClippedValue(flag));
			}
			values[i] += flag;
			flag += jump;
		}
		for (double d : areas) {
			sumAreas += d;
		}
		for (int i = 0; i < s; i++) {
			sumCont += (values[i] * areas[i]);
		}
		return sumCont / sumAreas;
	}

}
