package sdsu.cs657.stockTrading;

public interface FuzzyTerm {

	 double getDegreeOfMembership();

	 void clearDegreeOfMembership();

	 void maxDegreeOfMembership(double val);
}
