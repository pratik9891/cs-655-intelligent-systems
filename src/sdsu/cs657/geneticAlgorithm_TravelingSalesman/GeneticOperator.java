package sdsu.cs657.geneticAlgorithm_TravelingSalesman;

import java.util.ArrayList;

public interface GeneticOperator {

	public ArrayList<Chromosome> evolve();
	
}
