package sdsu.cs657.geneticAlgorithm_TravelingSalesman;

import java.util.ArrayList;

public class Crossover implements GeneticOperator {
	
	private Gene[] parent1Genes;
	private Gene[] parent2Genes;
	
	public Crossover(Chromosome parent1, Chromosome parent2) throws Exception{
		if(parent1.length() != parent2.length())
			throw new Exception("both parents should have same chromosomes!");
		parent1Genes = parent1.getGenes();
		parent2Genes = parent2.getGenes();
	}
	
	@Override
	public ArrayList<Chromosome> evolve() {
		int cutPostion = getRandomIndex(parent1Genes.length-2);
		int cutLength = 1+getRandomIndex(parent1Genes.length-1-cutPostion);
		int limit = cutLength + cutPostion;
		Gene[] child1 = new Gene[parent1Genes.length];
		Gene[] child2 = new Gene[parent2Genes.length];
		for(int j= cutPostion; j<limit; j++)
			child1[j] = parent1Genes[j];
		for(int j= cutPostion; j<limit; j++)
			child2[j] = parent2Genes[j];
		int j=0;
		for(int i=0; i<parent2Genes.length; i++){
			if(!isContainedIn(child1, parent2Genes[i].value())){
					int nextPosition = getNextAvailable(child1);
					if(nextPosition >=0)
						child1[nextPosition]= parent2Genes[i];
			}				
		}	
		for(int i=0; i<parent1Genes.length; i++){
			if(!isContainedIn(child2, parent1Genes[i].value())){
					int nextPosition = getNextAvailable(child2);
					if(nextPosition >=0)
						child2[nextPosition]= parent1Genes[i];
			}				
		}	
		child1[parent2Genes.length-1] = child1[0];
		child2[parent1Genes.length-1] = child2[0];
		Chromosome r1 = new Chromosome(child1);
		Chromosome r2 = new Chromosome(child2);
		ArrayList<Chromosome> result = new ArrayList<Chromosome>();
		result.add(r1);
		result.add(r2);
		return result;
	}
	
	
	private boolean isContainedIn(Gene[] input, int value){
		for(int i=0; i< input.length; i++){
			if(input[i] != null){
				if(input[i].value() == value)
					return true;
				}
		}
		return false;
	}
	
	private int getNextAvailable(Gene[] input){
		for(int i=0; i<input.length; i++){
			if(input[i] == null)
				return i;
		}
		return -1;
	}
	
	private int getRandomIndex(int limit){
		int result =  (int) (Math.random()*(limit));
		while(result == 0)
			result =  (int) (Math.random()*(limit));
		return result;
	}

}
