package sdsu.cs657.geneticAlgorithm_TravelingSalesman;

import java.util.ArrayList;
import java.util.Collections;

public class Shuffle implements GeneticOperator {
	
	Gene[] parentGenes;
	
	public Shuffle(Chromosome parent){
		parentGenes = parent.getGenes();
	}

	@Override
	public ArrayList<Chromosome> evolve() {
		ArrayList<Gene> child = new ArrayList<Gene>();
		for(int i=0; i<parentGenes.length-1; i++){
			child.add(parentGenes[i]);
		}
		Collections.shuffle(child);
		child.add(child.get(0));
		for(int i=0; i<parentGenes.length; i++){
			parentGenes[i] = child.get(i);
		}
		ArrayList<Chromosome> result = new ArrayList<Chromosome>();
		Chromosome r1= new Chromosome(parentGenes);
		result.add(r1);
		return result;
	}

}
