package sdsu.cs657.geneticAlgorithm_TravelingSalesman;

import java.util.ArrayList;

public class NReverse implements GeneticOperator {
	
	
	Gene[] parentGenes;
	
	public NReverse(Chromosome parent){
		parentGenes = parent.getGenes();
	}


	@Override
	public ArrayList<Chromosome> evolve() {
		int cutPostion = getRandomIndex(parentGenes.length-2);
		int cutLength = getRandomIndex(parentGenes.length-1-cutPostion);
		int limit = cutLength + cutPostion;
		Gene[] child = new Gene[parentGenes.length];
		for(int j =0; j<cutPostion; j++)
			child[j]=parentGenes[j];
		for(int j =limit; j<parentGenes.length; j++)
			child[j]=parentGenes[j];
		for(int j= cutPostion; j<=limit; j++){
			child[j] = parentGenes[limit];
			child[limit] = parentGenes[j];
			limit--;
		}
		child[parentGenes.length-1] = child[0];
		Chromosome r = new Chromosome(child);
		ArrayList<Chromosome> result = new ArrayList<Chromosome>();
		result.add(r);
		return result;
	}

	private int getRandomIndex(int limit){
		int result =  (int) (Math.random()*(limit));
		while(result == 0)
			result =  (int) (Math.random()*(limit));
		return result;
	}
}
