package sdsu.cs657.geneticAlgorithm_TravelingSalesman;

import java.util.ArrayList;

public class BestParentCrossover implements GeneticOperator {

	private Chromosome parent1;
	private Chromosome parent2;
	
	public BestParentCrossover (Chromosome parent1, Chromosome parent2) throws Exception{
		if(parent1.length() != parent2.length())
			throw new Exception("both parents should have same chromosomes!");
		this.parent1 = parent1;
		this.parent2 = parent2;
	}
	@Override
	public ArrayList<Chromosome> evolve() {
		ArrayList<Chromosome> result = new ArrayList<Chromosome>();
		if(parent1.getFitness() < parent2.getFitness())
			result.add(parent1);
		else
			result.add(parent2);
		return result;
	}

}
