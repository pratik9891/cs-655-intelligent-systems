package sdsu.cs657.geneticAlgorithm_TravelingSalesman;

import java.util.ArrayList;
import java.util.Collections;

import javax.lang.model.element.VariableElement;

public class TravelingSalesman {
	
	public static void main(String[] args) throws Exception{
		ArrayList<Chromosome> presentGeneration = new ArrayList<Chromosome>();
		for(int i=0; i<20; i++){
			Chromosome c = new Chromosome(25);
			c.generateRandom();
			presentGeneration.add(c);
		}

		
		ArrayList<Chromosome> nextgeneration = new ArrayList<Chromosome>();
		Chromosome min = presentGeneration.get(0);
		double presentAverageFitness =0;
		for(Chromosome each: presentGeneration)
			presentAverageFitness+=each.getFitness();
		presentAverageFitness = presentAverageFitness/presentGeneration.size();
		ArrayList<Chromosome> children = new ArrayList<Chromosome>();
		int crossover,best,shuf,mut,rev;
		crossover=best=shuf=mut=rev=0;
		int limit =0;
		
	//	for(int x=0; x<400; x++){//number of generations
		do{
			RouletteWheelSelection r = new RouletteWheelSelection();
			for(Chromosome each: presentGeneration)
				r.add(each);
			r.generatePieChart();
			for(int i=0; i<7; i++){//number of offsprings
				int randomChooser = (int) (Math.random()*100);

				if(randomChooser < 90){
					crossover++;
					GeneticOperator cross = new Crossover(r.getNext(), r.getNext());
					children.clear();
					children = cross.evolve();
					for (Chromosome each: children){
						nextgeneration.add(each);
					}
					
//				}else if(60<=randomChooser && randomChooser<75 ){
//					best++;
//					GeneticOperator bestParent = new BestParentCrossover(r.getNext(), r.getNext());
//					children.clear();
//					children = bestParent.evolve();
//					for (Chromosome each: children){
//						nextgeneration.add(each);
//					}
//					
//				}else if(75<=randomChooser && randomChooser<78 ){
//					shuf++;
//					GeneticOperator shuffle = new Shuffle(r.getNext());
//					children.clear();
//					children = shuffle.evolve();
//					for (Chromosome each: children){
//						nextgeneration.add(each);
//					}
//					
//				}else if(80 <= randomChooser && randomChooser < 85){
//					mut++;
//					GeneticOperator mutate = new Mutation(r.getNext());
//					children.clear();
//					children = mutate.evolve();
//					for (Chromosome each: children){
//						nextgeneration.add(each);
//					}
				}else{
//					rev++;
//					GeneticOperator nReverse = new NReverse(r.getNext());
//					children.clear();
//					children = nReverse.evolve();
//					for (Chromosome each: children){
//						nextgeneration.add(each);
//					}
					mut++;
					GeneticOperator mutate = new Mutation(r.getNext());
					children.clear();
					children = mutate.evolve();
					for (Chromosome each: children){
						nextgeneration.add(each);
					}

				}
			}
		
			Collections.sort(nextgeneration, new ChromosomeComparator());
			presentGeneration.clear();
			for(int i=0; i<7; i++)
				presentGeneration.add(nextgeneration.get(i));
			nextgeneration.clear();
			if(presentGeneration.get(0).getFitness() < min.getFitness()){
				min = presentGeneration.get(0);
			}
			//System.out.println(presentGeneration.get(0) + ":::"+presentGeneration.get(0).getFitness());
			double nextAverageFitness =0;
			for(Chromosome each: presentGeneration)
				nextAverageFitness+=each.getFitness();
			nextAverageFitness = nextAverageFitness/presentGeneration.size();
			
			
//			System.out.println(nextAverageFitness + "Limit:"+limit);
//			System.out.println("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
			
		//	System.out.println("Variance "+ TravelingSalesman.variance(presentGeneration) + "Fitness:" + presentGeneration.get(0).getFitness());
			System.out.println(nextAverageFitness);
//			if(TravelingSalesman.variance(presentGeneration) == 0.0){
//				for(Chromosome each: presentGeneration)
//					System.out.println(each.toString());
//			}
			
//			if(nextAverageFitness >= presentAverageFitness )
//				limit++;
//			else
//				limit=0;
//			presentAverageFitness = nextAverageFitness;
			limit++;
			
		}while(limit <400);
//		for(Chromosome each: presentGeneration){
//			System.out.println(each + ":::"+each.getFitness());
//		}
		System.out.println(min + ":::"+presentGeneration.get(0).getFitness());
		
	}
	
	public static double variance(ArrayList<Chromosome> generation){
		double sum = 0;
		for(Chromosome each: generation)
			sum+=each.getFitness();
		double avearge = sum/generation.size();
		double diffsum = 0;
		for(Chromosome each: generation)
			diffsum += ((each.getFitness() - avearge) * (each.getFitness() - avearge));
		double var = diffsum/generation.size();
		return var;
	}

}
