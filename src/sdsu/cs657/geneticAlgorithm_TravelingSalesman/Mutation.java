package sdsu.cs657.geneticAlgorithm_TravelingSalesman;

import java.util.ArrayList;

public class Mutation implements GeneticOperator {
	
	Gene[] parentGenes;
	
	public Mutation(Chromosome parent){
		parentGenes = parent.getGenes();
	}

	@Override
	public ArrayList<Chromosome> evolve() {	
		int randomGene1 = getRandomIndex();
		int randomGene2 = getRandomIndex();		
		while(randomGene1 == randomGene2)
			randomGene2 = getRandomIndex();
		Gene swap = parentGenes[randomGene1];
		parentGenes[randomGene1] = parentGenes[randomGene2];
		parentGenes[randomGene2] = swap;
		ArrayList<Chromosome> result = new ArrayList<Chromosome>();
		Chromosome r1= new Chromosome(parentGenes);
		result.add(r1);
		return result;
	}
	
	private int getRandomIndex(){
		return (int) (Math.random()*(parentGenes.length-1));
	}
	

}
