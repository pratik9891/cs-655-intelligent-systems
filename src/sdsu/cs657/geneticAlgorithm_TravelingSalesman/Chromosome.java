package sdsu.cs657.geneticAlgorithm_TravelingSalesman;

import java.util.ArrayList;

public class Chromosome{
	
	private double fitnessScore;
	private Gene[] genes;
	private int length;
	
	public Chromosome(int length){
		this.length = length;
		genes = new Gene[length];
		for(int i=0; i<length; i++){
			genes[i] = new Gene();
		}
	}
	
	public Chromosome(Gene[] g){
		this.genes = g;
		this.length = g.length;
	}
	
	public Chromosome(){
		this(10);
	}
	
	public int length(){
		return genes.length;
	}
	
	public Gene[] getGenes(){
		return genes;
	}
	
	public void evaluateFitness(){
		fitnessScore = 0;
		for(int i =0; i<genes.length-1 ; i++){
			fitnessScore+= genes[i].distanceFrom(genes[i+1]);
		}
		//System.out.println("Score: " + fitnessScore);
	}
	
	public double getFitness() {
		evaluateFitness();
		return this.fitnessScore;
	}
	
	public void generateRandom(){
		for(int i=0; i<length-1; i++){
			genes[i].set(i+1);
		}
		for(int i=0; i<length-1; i++)
			swap(genes, i, (int)(Math.random()*(length-1)));
		genes[length-1] = genes[0];	
	}
	
	private void swap(Gene[] genes2, int i, int j) {	
		Gene swap = genes2[j];
		genes2[j] = genes2[i];
		genes2[i] = swap;
	}
	
	public String toString(){
		String result = "";
		for(int i=0; i<length; i++){
			result += genes[i].value()+ " ";
		}
		return result;
	}
	
	public boolean equals(Chromosome another){
		if(this.length() != another.length())
			return false;
		if(this.toString().equals(another.toString()))
			return true;
		return false;
	}
	
	public static boolean validate(Chromosome c){
		if(c.getGenes()[0] != c.getGenes()[c.length-1])
			return false;
		Gene[] temp = c.getGenes();
		int sum=0;
		for(int i=0; i<temp.length-1; i++){
			sum += temp[i].value();
		}
		System.out.println(sum);
		if(sum!=300)
			return false;
		return true;
	}
	
	
	public static void main(String args[]) throws Exception{
		Chromosome test = new Chromosome(25);
		test.generateRandom();
		System.out.println(test.toString());
		Chromosome test1 = new Chromosome(25);
		test1.generateRandom();
		System.out.println(test1.toString());
		//Mutation
//		GeneticOperator m = new Mutation(test);
//		Chromosome child = m.evolve();
		//Crossover
//		GeneticOperator m = new Crossover(test, test1);
//		ArrayList<Chromosome> children = m.evolve();
//		for(Chromosome each:children){
//			System.out.println(each.toString());
//		}
		
		//nreverse
//		GeneticOperator m = new NReverse(test);
//		ArrayList<Chromosome> children = m.evolve();
//		for(Chromosome each:children){
//			System.out.println(each.toString());
//		}
		
		//Shuffle
//		GeneticOperator m = new Shuffle(test);
//		ArrayList<Chromosome> children = m.evolve();
//		for(Chromosome each:children){
//			System.out.println(each.toString());
//		}
		
		//BestParent
		GeneticOperator m = new BestParentCrossover(test,test1);
		ArrayList<Chromosome> children = m.evolve();
		for(Chromosome each:children){
			System.out.println(each.toString());
		}
		
		
		
		
	}


}
