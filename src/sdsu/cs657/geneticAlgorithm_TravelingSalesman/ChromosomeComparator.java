package sdsu.cs657.geneticAlgorithm_TravelingSalesman;

import java.util.Comparator;

public class ChromosomeComparator implements Comparator<Chromosome> {

	@Override
	public int compare(Chromosome o1, Chromosome o2) {
		return Double.valueOf(o1.getFitness()).compareTo(Double.valueOf(o2.getFitness()));
	}

}
