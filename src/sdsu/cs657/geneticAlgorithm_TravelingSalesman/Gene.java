package sdsu.cs657.geneticAlgorithm_TravelingSalesman;

public class Gene {

	private int value;
	
	public void set(int value){
		this.value = value;
	}
	
	public int value(){
		return this.value;
	}
	
	public double distanceFrom(Gene successor){
		return (this.value() - successor.value())*(this.value() - successor.value()) + 2*Math.sqrt(this.value()*successor.value());
	}
	
}
