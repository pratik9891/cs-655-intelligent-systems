package sdsu.cs657.geneticAlgorithm_TravelingSalesman;

import java.util.ArrayList;

public class RouletteWheelSelection {
		
	ArrayList<Chromosome> allChromosome = new ArrayList<Chromosome>();
	double[] ranges;
	double totalValue = 0;
	
	public void add(Chromosome c){
		allChromosome.add(c);
	}
	
	public void generatePieChart(){
		for(Chromosome each: allChromosome){
			totalValue += 1/each.getFitness();
		}
		ranges = new double[allChromosome.size()+1];
		ranges[0] = 0;
		for(int i=0; i<allChromosome.size(); i++){
			ranges[i+1] = ranges[i] + ((1/allChromosome.get(i).getFitness())*(360/totalValue));
		}
		
	}
	
	public Chromosome getNext(){
		int randomValue = (int) (Math.random() * 360);
		int i =0;
		while (!(randomValue < ranges[i]) && i < ranges.length)
			i++;
		return allChromosome.get(i-1);
	}
	
//	private int binarySearch(int value){
//		int first = 0;
//		int last = ranges.length;
//		int mid = 0;
//		while(first <= last){
//			mid = (first + last)/2;
//			if(value > ranges[mid])
//				first = mid;
//			if(value < ranges[mid])
//				mid = last;
//			if(value == ranges[mid])
//				return mid;
//		}	
//		return mid;
//		
//	}
	
	public void removeAll(){
		allChromosome.clear();
		totalValue = 0;
		for(int i=0; i<ranges.length; i++){
			ranges[i] = 0.0;
		}
	}
	
	
	public static void main(String args[]){
		Chromosome c1 = new Chromosome(25);
		c1.generateRandom();
		System.out.println("c1: "+c1.toString());
		Chromosome c2 = new Chromosome(25);
		c2.generateRandom();
		System.out.println("c2: "+c2.toString());
		Chromosome c3 = new Chromosome(25);
		c3.generateRandom();
		System.out.println("c3: "+c3.toString());
		RouletteWheelSelection r = new RouletteWheelSelection();
		r.add(c1);
		r.add(c2);
		r.add(c3);
		r.generatePieChart();
		int count1,count2,count3;
		count1 = count2 = count3 = 0;
		for(int i=0; i<10000; i++){
			if(r.getNext().toString().equals(c1.toString()))
				count1++;
			if(r.getNext().toString().equals(c2.toString()))
				count2++;
			if(r.getNext().toString().equals(c3.toString()))
				count3++;
			
		}
		System.out.println("count1: "+ count1);
		System.out.println("count2: "+ count2);
		System.out.println("count3: "+ count3);
	}


}
